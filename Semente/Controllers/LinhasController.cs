﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System.Linq.Expressions;
using Semente.DTO;

namespace Semente.Controllers
{
    [Produces("application/json")]
    [Route("api/Linhas")]
    public class LinhasController : Controller
    {
        private readonly SementeContext _context;

        public LinhasController(SementeContext context)
        {
            _context = context;
        }
        
        // GET: api/Linhas
        [HttpGet]
        public IQueryable<LinhaDTO> GetLinha()
        {
            return _context.Linha.Include(P => P.Receita).Select(asLinhaDTO) ;
        }

        // GET: api/Linhas/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLinha([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var linha = await _context.Linha.SingleOrDefaultAsync(m => m.Id == id);

            if (linha == null)
            {
                return NotFound();
            }

            return Ok(linha);


        }
    
        private static readonly Expression<Func<Linha, LinhaDTO>> asLinhaDTO = 
            x=>new LinhaDTO
            {
            Quantidade = x.Quantidade,
            Validade = x.Validade,
            DataReceita = x.Receita.Data
        };
       
        // PUT: api/Linhas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLinha([FromRoute] int id, [FromBody] Linha linha)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != linha.Id)
            {
                return BadRequest();
            }

            _context.Entry(linha).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LinhaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Linhas
        [HttpPost]
        public async Task<IActionResult> PostLinha([FromBody] Linha linha)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Linha.Add(linha);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLinha", new { id = linha.Id }, linha);
        }

        // DELETE: api/Linhas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLinha([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var linha = await _context.Linha.SingleOrDefaultAsync(m => m.Id == id);
            if (linha == null)
            {
                return NotFound();
            }

            _context.Linha.Remove(linha);
            await _context.SaveChangesAsync();

            return Ok(linha);
        }

        private bool LinhaExists(int id)
        {
            return _context.Linha.Any(e => e.Id == id);
        }
    }
}