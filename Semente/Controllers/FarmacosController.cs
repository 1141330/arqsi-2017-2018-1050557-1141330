﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System.Linq.Expressions;
using Semente.DTO;
using Microsoft.AspNetCore.Authorization;

namespace Semente.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Farmacos")]
    public class FarmacosController : Controller
    {
        private readonly SementeContext _context; 
            private static readonly Expression<Func<Farmaco, FarmacoDTO>> AsFarmacoDto =
            x => new FarmacoDTO
            {
                Id = x.Id,
                Nome = x.Nome
            };
        private static readonly Expression<Func<Medicamentos, MedicamentosDTO>> AsMedicamentosDTO =
            x => new MedicamentosDTO
            {
                Id = x.ID,
                Nome = x.Nome,
                Dosagem = x.Dosagem,
            };
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDto =
           x => new ApresentacaoDTO
           {
               Id = x.Id,
               Forma = x.Forma,
               Quantidade = x.Quantidade,
           };
        private static readonly Expression<Func<Posologia, PosologiaDTO>> AsPosologiaDto =
        x => new PosologiaDTO
    {
        Id = x.Id,
        Descricao = x.Descricao
    };
        public FarmacosController(SementeContext context)
        {
            _context = context;
        }

        // GET: api/Farmacos
        [HttpGet]
        public IEnumerable<Farmaco> GetFarmaco()
        {
            return _context.Farmaco;
        }

        // GET: api/Farmacos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);

            if (farmaco == null)
            {
                return NotFound();
            }

            return Ok(farmaco);
        }
        [Route("nome={nome}")]
        public IQueryable<FarmacoDTO> GetFarmacoByNome([FromRoute]string nome)
        {
            return _context.Farmaco
                .Where(b => b.Nome.Equals(nome, StringComparison.OrdinalIgnoreCase))
                .Select(AsFarmacoDto);
        }
        [Route("{id}/Apresentacoes")]
        public IQueryable<ApresentacaoDTO> GetApresentacoesFarmacoByID(int id)
        {
            return _context.Apresentacao.Where(a => a.FarmacoId == id).Select(AsApresentacaoDto);
        }

        [Route("{id}/Medicamentos")]

        public List<MedicamentosDTO> GetMedicamentosByFarmacoID(int id)
        {
            IQueryable<MedicamentosDTO> medicametosFarmaco = _context.Medicamentos.Where(m => m.ID == id).Select(AsMedicamentosDTO);
            List<MedicamentosDTO> retorno = new List<MedicamentosDTO>();
         
            foreach (MedicamentosDTO med in medicametosFarmaco)
            {
                IQueryable<MedicamentosDTO> medicamentos = _context.Medicamentos.Where(m => m.ID == med.Id).Select(AsMedicamentosDTO);
                foreach(MedicamentosDTO med1 in medicamentos)
                {
                    retorno.Add(med1);
                }
            }
            return retorno;
        }


        [Route("{id}/Posologias")]
        public List<PosologiaDTO> GetPosologiasFarmacoByID(int id)
        {
            IQueryable<ApresentacaoDTO> apresentacoesFarmaco = _context.Apresentacao.Where(a => a.FarmacoId == id).Select(AsApresentacaoDto);
            List<PosologiaDTO> retorno = new List<PosologiaDTO>();

            foreach (ApresentacaoDTO apr in apresentacoesFarmaco)
            {
                IQueryable<PosologiaDTO> posologias = _context.Posologia.Where(p => p.ApresentacaoId == apr.Id).Select(AsPosologiaDto);
                foreach (PosologiaDTO poso in posologias)
                {
                    retorno.Add(poso);
                }
            }

            return retorno;
        }
        // PUT: api/Farmacos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFarmaco([FromRoute] int id, [FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != farmaco.Id)
            {
                return BadRequest();
            }

            _context.Entry(farmaco).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FarmacoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Farmacos
        [HttpPost]
        public async Task<IActionResult> PostFarmaco([FromBody] Farmaco farmaco)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Farmaco.Add(farmaco);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFarmaco", new { id = farmaco.Id }, farmaco);
        }

        // DELETE: api/Farmacos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFarmaco([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var farmaco = await _context.Farmaco.SingleOrDefaultAsync(m => m.Id == id);
            if (farmaco == null)
            {
                return NotFound();
            }

            _context.Farmaco.Remove(farmaco);
            await _context.SaveChangesAsync();

            return Ok(farmaco);
        }

        private bool FarmacoExists(int id)
        {
            return _context.Farmaco.Any(e => e.Id == id);
        }
    }
}