﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using Microsoft.AspNetCore.Routing;
using System.Linq.Expressions;
using Semente.DTO;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Authorization;
namespace Semente.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/Medicamentos")]
    public class MedicamentosController : Controller
    {
        private readonly SementeContext _context;
        private static readonly Expression<Func<Apresentacao, ApresentacaoDTO>> AsApresentacaoDto =
           x => new ApresentacaoDTO
           {
               Id = x.Id,
               Forma = x.Forma,
               Quantidade = x.Quantidade,
           };
        private static readonly Expression<Func<Posologia, PosologiaDTO>> AsPosologiaDto =
    x => new PosologiaDTO
    {
        Id = x.Id,
        Descricao = x.Descricao
    };
        public MedicamentosController(SementeContext context)
        {
            _context = context;
        }

        // GET: api/Medicamentos
        [HttpGet]
        public IEnumerable<Medicamentos> GetMedicamentos()
        {
            return _context.Medicamentos;
        }

        // GET: api/Medicamentos/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetMedicamentos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamentos = await _context.Medicamentos.SingleOrDefaultAsync(m => m.ID == id);

            if (medicamentos == null)
            {
                return NotFound();
            }

            return Ok(medicamentos);
        }
        [HttpGet("nome={nome:alpha}")]
        public async Task<IActionResult> GetMedicamentosByNome([FromRoute] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamentos = await _context.Medicamentos.SingleOrDefaultAsync(m => m.Nome.ToLower() == nome.ToLower());

            if (medicamentos == null)
            {
                return NotFound();
            }

            return Ok(medicamentos);
        }

        // Duvidaaaaaaaaaaaaaaaaaaaa : COMO RETORNAR OK(Iqueryable);
       
        [Route("{id}/Apresentacoes")]
        public  IQueryable<ApresentacaoDTO> GetMedicamentosByApresentacao([FromRoute] int id)
        {
             
           return _context.Apresentacao.Where(a => a.MedicamentosID==id).Select(AsApresentacaoDto);
            
        }
        [Route("{id}/Posologias")]
        public HashSet<PosologiaDTO> GetPosologiasMedicamentoByID(int id)
        {
            IQueryable<ApresentacaoDTO> apresentacoesMedicamento = _context.Apresentacao.Where(a => a.MedicamentosID == id).Select(AsApresentacaoDto);
            HashSet<PosologiaDTO> retorno = new HashSet<PosologiaDTO>();

            foreach (ApresentacaoDTO apr in apresentacoesMedicamento)
            {
                IQueryable<PosologiaDTO> posologias = _context.Posologia.Where(p => p.ApresentacaoId == apr.Id).Select(AsPosologiaDto);
                foreach (PosologiaDTO poso in posologias)
                {
                    retorno.Add(poso);
                }
            }

            return retorno;
        }


        // PUT: api/Medicamentos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedicamentos([FromRoute] int id, [FromBody] Medicamentos medicamentos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medicamentos.ID)
            {
                return BadRequest();
            }

            _context.Entry(medicamentos).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicamentosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medicamentos
        [HttpPost]
        public async Task<IActionResult> PostMedicamentos([FromBody] Medicamentos medicamentos)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Medicamentos.Add(medicamentos);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedicamentos", new { id = medicamentos.ID }, medicamentos);
        }

        // DELETE: api/Medicamentos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedicamentos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medicamentos = await _context.Medicamentos.SingleOrDefaultAsync(m => m.ID == id);
            if (medicamentos == null)
            {
                return NotFound();
            }

            _context.Medicamentos.Remove(medicamentos);
            await _context.SaveChangesAsync();

            return Ok(medicamentos);
        }

        private bool MedicamentosExists(int id)
        {
            return _context.Medicamentos.Any(e => e.ID == id);
        }
    }
}