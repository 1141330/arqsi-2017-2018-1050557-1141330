﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Semente.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Semente.Models
{
    public class SementeContext : IdentityDbContext<UserEntity>

    {
        public SementeContext (DbContextOptions<SementeContext> options)
            : base(options)
        {
        }

        public DbSet<Semente.Models.Medicamentos> Medicamentos { get; set; }

        public DbSet<Semente.Models.Apresentacao> Apresentacao { get; set; }

        public DbSet<Semente.Models.Linha> Linha { get; set; }

        public DbSet<Semente.Models.Farmaco> Farmaco { get; set; }

        public DbSet<Semente.Models.Posologia> Posologia { get; set; }

        public DbSet<Semente.Models.Receita> Receita { get; set; }
    }
}

