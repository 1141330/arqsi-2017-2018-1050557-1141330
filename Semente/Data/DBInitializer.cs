﻿using Microsoft.EntityFrameworkCore;
using Semente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Semente.Data
{
    public class DBInitializer
    {
        public static void Initialize(SementeContext context)
        {
            context.Database.EnsureCreated();
            if(context.Farmaco.Any())
            {
                return;
            }
                var DadosFarmaco = new Farmaco[]
                    {
                        new Farmaco{Nome="Paractemol"},
                        new Farmaco{Nome="VitaminaC"},
                        new Farmaco{Nome="VitaminaD"},

                    };
                    foreach (Farmaco f in DadosFarmaco)
                        context.Farmaco.Add(f);
                context.SaveChanges();
                    var DadosMedicamento = new Medicamentos[]
                    {
                        new Medicamentos{Nome = "Benuron", Dosagem="200ml"},
                        new Medicamentos{Nome = "Paracetmol", Dosagem="400ml"}
                    };

            foreach (Medicamentos m in DadosMedicamento)
                context.Medicamentos.Add(m);
            context.SaveChanges();

                    var DadosApresentacao = new Apresentacao[]
                    {
                        new Apresentacao{FarmacoId=1,Forma="Oral",Concentracao=12,MedicamentosID=1,Quantidade=2},
                        new Apresentacao{FarmacoId=1,Forma="Intravenoso",Concentracao=14,MedicamentosID=1,Quantidade=2},

                    };
                    foreach (Apresentacao a in DadosApresentacao)
                        context.Apresentacao.Add(a);
                    context.SaveChanges();
            var DadosPosologia = new Posologia[]
{
                    new Posologia{Descricao="5 em 5 dias",ApresentacaoId=1},
                    new Posologia{Descricao="1 em 1 dias",ApresentacaoId=1},
                    new Posologia{Descricao="24 em 24 horas",ApresentacaoId=2},
                    new Posologia{Descricao="12 em 12 horas",ApresentacaoId=2},
};
            foreach (Posologia p in DadosPosologia)
                context.Posologia.Add(p);
                context.SaveChanges();



        }
    }
    }

