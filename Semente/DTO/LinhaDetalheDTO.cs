﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class LinhaDetalheDTO
    {
        public int Quantidade { get; set; }
        public DateTime Validade { get; set; }
        public DateTime Date { get; set; }
        public int ApresentacaoID { get; set; }
    }
}
