﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class LinhaDTO
    {
        public int Quantidade { get; set; }
        public DateTime Validade { get; set; }
        public DateTime DataReceita { get; set; }
    }
}
