﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class ApresentacaoDTO
    {
        public int Id { get; set; }
        public string Forma { get; set; }
        public int Quantidade { get; set; }
    }
}
