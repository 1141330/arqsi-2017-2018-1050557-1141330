﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class PrescicaoDetalheDto
    {
        public int Quantidade { get; set; }
        public DateTime Validade { get; set; }
        public DateTime Data { get; set; }
        public int AprestancaoID { get; set; }
    }
}
