﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.DTO
{
    public class MedicamentosDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Dosagem { get; set; }
    }
}
