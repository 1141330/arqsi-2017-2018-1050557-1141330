﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class Linha
    {
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public DateTime Validade { get; set; }
        public int ReceitaId { get; set; }
        public Receita Receita { get; set; }
        public int ApresentacaoId { get; set; }
        public Apresentacao Apresentacao { get; set; }
    }
}
