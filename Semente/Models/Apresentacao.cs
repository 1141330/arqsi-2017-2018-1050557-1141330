﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class Apresentacao
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Quantidade { get; set; }
        public string Forma { get; set; }
        public int Concentracao { get; set; }
        public int FarmacoId { get; set; }
        public Farmaco Farmaco { get; set; }
        public int MedicamentosID { get; set; }
        public Medicamentos Medicamentos { get; set; }
        public ICollection<Posologia> Posologias { get; set; }
    }
}
