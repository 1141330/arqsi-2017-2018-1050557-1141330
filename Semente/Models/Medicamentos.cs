﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class Medicamentos
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Dosagem { get; set; }
        public ICollection<Apresentacao> Apresentacoes { get;set; }
        
    }
}
