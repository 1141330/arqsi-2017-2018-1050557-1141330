﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Semente.Models
{
    public class Receita
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Prescritor { get; set; }
        public string Utente { get; set; }
        public ICollection<Linha> Linhas { get; set; }

    }
}
